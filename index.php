<?php
$link = mysqli_connect("localhost", "root", "", "db");

if (!$link) {
    echo "Ошибка: Невозможно установить соединение с MySQL." . PHP_EOL;
    echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

?>
<!DOCTYPE html>
<html>
<style>
/* Full-width input fields */
input[type=text], input[type=password] , input[type=date] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

/* Set a style for all buttons */
button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

/* Extra styles for the cancel button */
.cancelbtn {
    padding: 14px 20px;
    background-color: #f44336;
}

/* Float cancel and signup buttons and add an equal width */
.cancelbtn,.signupbtn {
    float: left;
    width: 50%;
}

/* Add padding to container elements */
.container {
    padding: 16px;
}

/* Clear floats */
.clearfix::after {
    content: "";
    clear: both;
    display: table;
}
.error {color: #FF0000;}
/* Change styles for cancel button and signup button on extra small screens */
@media screen and (max-width: 300px) {
    .cancelbtn, .signupbtn {
       width: 100%;
    }
}
</style>
<body>

<h2>Signup Form</h2>
<?php
$chekBoxErr = $passReapeatErr = $userValidErr = $emailErr = $nameErr = $lastNameErr = $userEmptyErr = $ageErr = $tamamE = " ";
echo "Sksec <br />";
if(isset($_POST['submit'])){
	echo "mtav <br />";
	// chek if not empty USERNAME
	// chek if
    $userName=$_POST["userName"];
    $name=$_POST["name"];
    $lastName=$_POST["lastName"];
    $email_a=$_POST["email_a"];
    $psw=$_POST["psw"];
    $b_day=$_POST["birthday"];
    $tips=$_POST["tips"];
    $news=$_POST["news"];
	$_POST['userName'] = trim($_POST['userName']);
    $patternForUserName="/^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/";
	$patternForEmail = "/[a-zA-z0-9_\-\.]{6,}@[a-zA-Z]+\.[a-zA-Z]+/";
	$patternForName ="/^[a-zA-Z'-]+$/";
	if (
        !empty($_POST['userName'])
            &&
        !empty($_POST['name'])
            &&
        !empty($_POST['lastName'])
            &&
        !empty($_POST['email_a'])
            &&
        !empty($_POST['userName'])
            &&
        !empty($_POST['psw'])
            &&
        !empty($_POST['rpsw'])
            &&
        !empty($_POST['birthday'])
        ) {

        if (preg_match($patternForName, $_POST['name'])) {

            if (preg_match($patternForName, $_POST['lastName'])) {

        		if (preg_match($patternForEmail , $_POST['email_a'])) {

        			if (preg_match($patternForUserName, $_POST['userName'])) {

        				if ($_POST['psw']===$_POST['rpsw']) {

        					if ($_POST['tips'] or $_POST['newslatters']) {

        						preg_match_all('/(.*?)-/',$_POST['birthday'], $matches);
                                $matches=$matches[1][0];

                                if ( (date("Y")-$matches)>16 and  $matches>1900 and  $matches<date("Y")) {
                                    
                                    $tamamE="Sagha tamam e  <br />";



                                    $con="INSERT INTO mardiq (`username`,`psw`,`f_name`,`l_name`,`e_mail`,`b_day`,`tips`,`news`) 
                                          VALUES ('$userName','$name','$lastName','$email_a','$psw','$b_day','$tips','$news')";

                                    if (mysqli_query($link, $con)) {
                                        echo "New record created successfully";
                                        mysqli_query($link,$con);
                                    } else {
                                        echo "Error: " . $con . "<br>". "<br>". "<br>" . mysqli_error($link);
                                    }

                                    mysqli_close($link);

                                }
                                else
                                    $ageErr = "Tariqit het xndir ka  <br />";

        					}
        					else
        						$chekBoxErr = "Nshe gone mek@  <br />";
        				}
        				else
        					$passReapeatErr = "erku angam nuynic gre  <br />";

        			}
        			else	
        				$userValidErr = "USERNAME-i mej krna eghni tar u tiv <br />";
                }
                else
                    $emailErr = "EMAIL sxal es grel <br />";
            }
            else
                $lastNameErr = "LAASTNAME-@T sxal es grel <br />";
        }
        else
            $nameErr = "NAME-@t sxal es grel <br />";
	}
	else
		$userEmptyErr = "SaghTogher@ lracru <br />";
		
}
else
	echo " Ashxatel e <br />";

?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" style="border:1px solid #ccc">

  <div class="container">

    <label><b>Username</b></label>
    	<input type="text" placeholder="Enter Username" name="userName" >
        <span class="error">* <?php echo $userValidErr."<br />".$userEmptyErr;?></span>

    <label><b>Name</b></label>
    	<input type="text" placeholder="Enter Name" name="name">
        <span class="error">* <?php echo $nameErr;?></span>

    <label><b>Last Name</b></label>
    	<input type="text" placeholder="Enter Last Name" name="lastName">
        <span class="error">* <?php echo $lastNameErr;?></span>

    <label><b>Email</b></label>
    	<input type="text" placeholder="Enter Email" name="email_a" >
        <span class="error">* <?php echo $emailErr;?></span>

    <label><b>Password</b></label>
    	<input type="password" placeholder="Enter Password" name="psw" >

    <label><b>Repeat Password</b></label>
    	<input type="password" placeholder="Repeat Password" name="rpsw" >
        <span class="error">* <?php echo $passReapeatErr;?></span>

	<label><b>Birthday</b></label><br>
	    	<input type="date" placeholder="Set a Date" name="birthday" >
        <span class="error">* <?php echo $ageErr;?></span>

    <label><b>Subscription</b></label><br>
	    <input type="hidden" name="tips" value="0">
	    <input type="checkbox" name="tips" value="1">Tips and discounts<br>

	    <input type="hidden" name="news" value="0">
	    <input type="checkbox" name="news" value="1">Newslatters<br>

        <span class="error">* <?php echo $chekBoxErr;?></span>
    <br>

    <div class="clearfix">
      <button type="button" class="cancelbtn">Cancel</button>
      <button  type="submit" class="signupbtn" name="submit">Sign Up</button>
    </div>

  </div>

</form>

</body>
</html>
